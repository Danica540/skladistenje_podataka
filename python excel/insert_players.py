import random
import codecs
import csv

filename = "insert_players_sql.txt"

maxVisina = 195
minVisina = 160

maxTezina = 120
minTezina = 50

insertIntoFudbaler_Statement = "INSERT INTO `mydb`.`fudbaler` (`ime`, `prezime`, `visina`, `tezina`,`pozicija`,`zemlja_porekla`,`drzavljanstvo`,`godina_rodjenja`,`bmi`) VALUES ("


with open('names.csv', 'r', encoding="utf8") as myfile:
    for line in myfile:
        line = line.replace('\n', "")
        ime_tmp = line.split()
        ime_fudbalera = ""
        prezime_fudbalera = ""
        ime_fudbalera = ime_tmp[0]
        if len(ime_tmp) == 2:
            prezime_fudbalera = ime_tmp[1]

        visina_fudbalera = random.randint(minVisina, maxVisina)
        tezina_fudbalera = random.randint(minTezina, maxTezina)

        random_drzava = random.randint(0, 49)
        zemlja_porekla = ""
        drzavljanstvo = ""
        random_drzavljanstvo = random.uniform(0, 1)

        a_file = open("drzave.txt", encoding="utf-8")
        for position, line_d in enumerate(a_file):
            if position == random_drzava:
                zemlja_porekla = line_d
        a_file.close()

        if random_drzavljanstvo <= 0.1:
            random_drzava_2 = random.randint(0, 49)
            a_file = open("drzave.txt", encoding="utf-8")
            for position, line_d in enumerate(a_file):
                if position == random_drzava_2:
                    drzavljanstvo = line_d
            a_file.close()
        else:
            drzavljanstvo=zemlja_porekla

        pozicija = ""
        random_pozicija = random.randint(0, 15)
        a_file = open("unique_positions.txt", encoding="utf-8")
        for position, line_d in enumerate(a_file):
            if position == random_pozicija:
                pozicija = line_d
        a_file.close()

        godina_rodjenja = random.randint(1950, 1988)
        bmi=tezina_fudbalera/((visina_fudbalera/100)*(visina_fudbalera/100))

        # `ime`, `prezime`, `visina`, `tezina`,`pozicija`,`zemlja_porekla`,`drzavljanstvo`,`godina_rodjenja`,`bmi`
        toWrite = insertIntoFudbaler_Statement
        toWrite += "\""+ime_fudbalera+"\"" + ", " + "\""+prezime_fudbalera+"\"" + ", " + str(visina_fudbalera)  + ", " + str(tezina_fudbalera)+", " + \
            "\""+pozicija+"\"" + ", " + "\""+zemlja_porekla + "\""+", " + "\"" + \
            drzavljanstvo+"\"" + ", " + str(godina_rodjenja)+", " + str(bmi) + ");"
        fw = codecs.open(filename, "a", "utf-8")
        fw.write(toWrite)
        fw.write("\n\n")

myfile.close()
