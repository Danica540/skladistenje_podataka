import random
import codecs
import csv

filename = "insert_transfers_sql.txt"

insertIntoFudbalerHasTim_Statement = "INSERT INTO `mydb`.`fudbaler_has_tim` (`Fudbaler_idFudbaler`, `Tim_idTim`, `procenjena_cena`, `realna_cena`, `Sezona_idSezona`,`Liga_idLiga`,`trajanje_ugovora`) VALUES ("


with open('transferi.csv', 'r', encoding="utf8") as csv_file:
    csv_reader = csv.reader(csv_file)
    next(csv_reader)
    for line in csv_reader:
        # id fudbalera
        name = line[0]
        fudbaler_id = 1
        a_file = open("names.csv", encoding="utf8")
        for position, line_d in enumerate(a_file):
            line_d = line_d.replace('\n', "")
            if line_d == name:
                fudbaler_id = position+1
        a_file.close()

        # team_from - id team
        team_from = line[3]
        team_id = -9
        a_file = open("unique_teams.txt", encoding="utf8")
        for position, line_d in enumerate(a_file):
            line_d = line_d.replace('\n', "")
            if line_d == team_from:
                team_id = position+1
        a_file.close()

        ime_lige = line[4]
        idLiga=-11
        a_file = open("unique_leagues.txt", encoding="utf8")
        for position, line_d in enumerate(a_file):
            line_d = line_d.replace("\n", "")
            if line_d == ime_lige:
                idLiga = position+1
        a_file.close()

        if(team_id == -9):
            print("tim   " + team_from)
        if(idLiga == -11):
            print("liga   " +ime_lige)

        # sezona - id sezone
        sezona = line[7]
        sezona_id = 1
        a_file = open("unique_seasons.txt", encoding="utf8")
        for position, line_d in enumerate(a_file):
            line_d = line_d.replace('\n', "")
            if line_d == sezona:
                sezona_id = position+1
        a_file.close()
        random_sezona=random.randint(0,100)
        if(random_sezona<45):
            sezona_id=sezona_id+1


        # aditivne vrednosti
        market_value = line[8]
        transfer_fee = line[9]
        if market_value == "NA" and transfer_fee != "NA":
            market_value = transfer_fee
        if market_value != "NA" and transfer_fee == "NA":
            transfer_fee = market_value
        if market_value == "NA" and transfer_fee == "NA":
            random_market = random.randint(1000000, 15000000)
            random_transfer = random.randint(1000000, 15000000)
            transfer_fee = random_transfer
            market_value = random_market

        trajanje_ugovora = random.randint(1, 5)
        # `Fudbaler_idFudbaler`, `Tim_idTim`, `market_value`, `transfer_fee`, `Sezona_idSezona`,Liga_idLiga, trajanje_ugovora
        toWrite = insertIntoFudbalerHasTim_Statement
        toWrite += str(fudbaler_id)+", " + str(team_id)+", " + str(market_value) + \
            ", " + str(transfer_fee)+", " + \
            str(sezona_id) + ", "+str(idLiga)+", "+str(trajanje_ugovora)+");"
        fw = codecs.open(filename, "a", "utf-8")
        fw.write(toWrite)
        fw.write("\n\n")

        # team_to - id team
        team_to = line[5]
        team_id = -9
        a_file = open("unique_teams.txt", encoding="utf8")
        for position2, line_d in enumerate(a_file):
            line_d = line_d.replace('\n', "")
            if line_d == team_to:
                team_id = position2+1
        a_file.close()

        ime_lige = line[6]
        idLiga=-11
        a_file = open("unique_leagues.txt", encoding="utf8")
        for position1, line_d in enumerate(a_file):
            line_d = line_d.replace("\n", "")
            if line_d == ime_lige:
                idLiga = position1+1
        a_file.close()

        if(team_id == -9):
            print("tim   " +team_to)
        if(idLiga == -11):
            print("liga   " +ime_lige)
        # `Fudbaler_idFudbaler`, `Tim_idTim`, `market_value`, `transfer_fee`, `Sezona_idSezona` Liga_idLiga, trajanje_ugovora
        toWrite = insertIntoFudbalerHasTim_Statement
        toWrite += str(fudbaler_id)+", " + str(team_id)+", " + str(market_value) + \
            ", " + str(transfer_fee)+", " + \
            str(sezona_id) + ", "+str(idLiga)+", "+str(trajanje_ugovora)+");"
        fw = codecs.open(filename, "a", "utf-8")
        fw.write(toWrite)
        fw.write("\n\n")

csv_file.close()
