import random
import codecs


filename = "test.txt"

maxVisina = 195
minVisina = 160

maxTezina = 120
minTezina = 50

ime_fudbalera = ""
prezime_fudbalera = ""
godine_fudbalera = 0
visina_fudbalera = 0
tezina_fudbalera = 0

insertIntoPozicija_Statement = "INSERT INTO `mydb`.`pozicija` (`naziv`) VALUES ("
insertIntoFudbaler_Statement = "INSERT INTO `mydb`.`fudbaler` (`ime`, `prezime`, `godine`, `visina`, `tezina`,`Pozicija_idPozicija`) VALUES ("
insertIntoTim_Statement = "INSERT INTO `mydb`.`tim` (`naziv`, `Liga_idLiga`) VALUES ("
insertIntoLiga_Statement = "INSERT INTO `mydb`.`liga` (`naziv`) VALUES ("
insertIntoSezona_Statement = "INSERT INTO `mydb`.`sezona` (`godina`) VALUES ("
insertIntoFudbalerHasTim_Statement = "INSERT INTO `fudbaler_has_tim`.`fudbaler` (`Fudbaler_idFudbaler`, `Tim_idTim`, `market_value`, `transfer_fee`, `Sezona_idSezona`) VALUES ("

with open('names.txt', 'r') as myfile:
    imena = myfile.readlines()
myfile.close()
with open('ages.txt', 'r') as myfile:
    godine = myfile.readlines()
myfile.close()
with open('league_from.txt', 'r') as myfile:
    liga_from = myfile.readlines()
myfile.close()
with open('league_to.txt', 'r') as myfile:
    liga_to = myfile.readlines()
myfile.close()
with open('team_from.txt', 'r') as myfile:
    tim_from = myfile.readlines()
myfile.close()
with open('team_to.txt', 'r') as myfile:
    tim_to = myfile.readlines()
myfile.close()
with open('market_values.txt', 'r') as myfile:
    market = myfile.readlines()
myfile.close()
with open('positions.txt', 'r') as myfile:
    pozicije = myfile.readlines()
myfile.close()
with open('season.txt', 'r') as myfile:
    sezone = myfile.readlines()
myfile.close()
with open('transfer_fees.txt', 'r') as myfile:
    transfer = myfile.readlines()
myfile.close()

num_lines = sum(1 for line in open('names.txt'))

for i in range(num_lines):
    ime_fudbalera = ""
    prezime_fudbalera = ""
    godine_fudbalera = 0
    visina_fudbalera = 0
    tezina_fudbalera = 0
    pozicija_fudbalera = ''

    line = imena[i].replace('\n', "")
    ime_tmp = line.split()
    ime_fudbalera = ime_tmp[0]
    if len(ime_tmp) == 2:
        prezime_fudbalera = ime_tmp[1]

    godine_fudbalera = godine[i]
    visina_fudbalera = random.randint(minVisina, maxVisina)
    tezina_fudbalera = random.randint(minTezina, maxTezina)
    pozicija_fudbalera = pozicije[i]

    toWrite = insertIntoPozicija_Statement
    toWrite += "\""+pozicija_fudbalera+"\"" + ");"
    fw = codecs.open(filename, "a", "utf-8")
    fw.write(toWrite)
    fw.write("\n\n")
    pozicija_id = i+1

    toWrite = insertIntoFudbaler_Statement
    toWrite += "\""+ime_fudbalera+"\"" + ", " + "\""+prezime_fudbalera+"\"" + ", " + \
        str(godine_fudbalera) + ", " + str(visina_fudbalera) + ", " + \
        str(tezina_fudbalera)+", " + str(pozicija_id) + ");"

    fw = codecs.open(filename, "a", "utf-8")
    fw.write(toWrite)
    fw.write("\n\n")
    fw.close()

for i in range(num_lines):
    sezona_godina = "2001-2002"
    sezona_godina = sezone[i]
    toWrite = insertIntoSezona_Statement
    toWrite += "\""+sezona_godina+"\"" + ");"

    fw = codecs.open(filename, "a", "utf-8")
    fw.write(toWrite)
    fw.write("\n\n")
    fw.close()

for i in range(num_lines):
    prva_liga_id = i+1
    liga_naziv = ""
    liga_naziv = liga_from[i]
    toWrite = insertIntoLiga_Statement
    toWrite += "\""+liga_naziv+"\"" + ");"
    fw = codecs.open(filename, "a", "utf-8")
    fw.write(toWrite)
    fw.write("\n\n")
    tim_naziv = ""
    tim_naziv = tim_from[i]
    toWrite = insertIntoTim_Statement
    toWrite += "\""+tim_naziv+"\""+", " + str(prva_liga_id)+");"
    fw = codecs.open(filename, "a", "utf-8")
    fw.write(toWrite)
    fw.write("\n\n")

    druga_liga_id = i+2
    liga_naziv = ""
    liga_naziv = liga_to[i]
    toWrite = insertIntoLiga_Statement
    toWrite += "\""+liga_naziv+"\"" + ");"
    fw = codecs.open(filename, "a", "utf-8")
    fw.write(toWrite)
    fw.write("\n\n")
    tim_naziv = ""
    tim_naziv = tim_to[i]
    toWrite = insertIntoTim_Statement
    toWrite += "\""+tim_naziv+"\"" +", "+ str(druga_liga_id)+");"
    fw = codecs.open(filename, "a", "utf-8")
    fw.write(toWrite)
    fw.write("\n\n")

    # team from
    market_value = market[i]
    transfer_fee = transfer[i]
    id_fudbalera = i+1

    toWrite = insertIntoFudbalerHasTim_Statement
    toWrite += str(id_fudbalera)+", "+str(i+1) +", "+ str(market_value)+", " + \
        str(transfer_fee)+", " + str(i+1) + ");"
    fw = codecs.open(filename, "a", "utf-8")
    fw.write(toWrite)
    fw.write("\n\n")
    # team to
    toWrite = insertIntoFudbalerHasTim_Statement
    toWrite += str(id_fudbalera)+", "+str(i+2)+", " + str(market_value)+", " + \
        str(transfer_fee)+", " + str(i+1) + ");"
    fw = codecs.open(filename, "a", "utf-8")
    fw.write(toWrite)
    fw.write("\n\n")
    fw.close()
