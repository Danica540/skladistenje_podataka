import random
import codecs

filename = "insert_leagues_sql.txt"

max_godina_osnivanja = 1999
min_godina_osnivanja = 1850

insertIntoLiga_Statement = "INSERT INTO `mydb`.`liga` (`naziv`,`godina_osnivanja`,`drzava`,`konfederacija`) VALUES ("

with open('unique_leagues.txt', 'r', encoding="utf8") as myfile:
    for position, line in enumerate(myfile):
        naziv = line.replace('\n', "")
        konfederacija = ""
        godina_osnivanja = random.randint(
            min_godina_osnivanja, max_godina_osnivanja)
        drzava = ""
        procenat = random.uniform(0, 1)
        if procenat >= 0 and procenat <= 0.79:
            konfederacija = "FIFA"
        if procenat >= 0.8 and procenat <= 0.91:
            konfederacija = "NF-Board"
        if procenat >= 0.92:
            konfederacija = "ConIFA"
        random_drzava = position
        a_file = open("drzave.txt", encoding="utf8")
        line_to_read = random_drzava

        for position, line_d in enumerate(a_file):
            if position == line_to_read:
                line_d = line_d.replace('\n', "")
                drzava = line_d
        a_file.close()

        toWrite = insertIntoLiga_Statement
        toWrite += "\""+naziv+"\"" + ", " + \
            str(godina_osnivanja) + ", " + "\""+drzava + \
            "\"" + ", " + "\""+konfederacija+"\"" + ");"
        fw = codecs.open(filename, "a", "utf-8")
        fw.write(toWrite)
        fw.write("\n\n")

myfile.close()
