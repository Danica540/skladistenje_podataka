﻿import random
import requests
import json 
import urllib3
import codecs
import datetime
import radar
import time

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

urlMale = "http://api.namefake.com/serbian-latin-serbia/male/"
urlFemale = "http://api.namefake.com/serbian-latin-serbia/female/"

status = ["Nepregledan", "Prosleđen", "Rešen", "Odbijen"]

filename = "C:\\Users\\Toni\\Desktop\\insertIntoProblemsNOVO.txt"

tip = ["Vodovod", "Struja", "Kanalzacija", "Smeće, kante, kontejneri", "Deponija",
"Atmosferska kanalizacija", "Saobraćajna signalizacija", "Putna infrastruktura", 
"Nepropisno parkiranje", "Insekti, glodari, gmizavci", "Uginule životinje",
"Psi lutalice", "Vandalizam", "Prekomerna buka", "Kućni red i održavanje stambenih zgrada",
"Zelene površine", "Javna rasveta", "Dečija igrališta", "Ekološki problem", 
"Kontrola nelegalnih građevinskih radova", "Zimska služba", "Oštećena fasada",
"Predlog", "Neki drugi problem"]

opstine = ["Grad Subotica",
        "Bačka Topola", 
        "Mali Iđoš",
        "Grad Zrenjanin", 
        "Novi Bečej", 
        "Nova Crnja", 
        "Žitište", 
        "Sečanj",
        "Kanjiža", 
        "Senta", 
        "Ada", 
        "Čoka", 
        "Novi Kneževac", 
        "Grad Kikinda",
        "Grad Pančevo", 
        "Plandište", 
        "Opovo", 
        "Kovačica", 
        "Alibunar", 
        "Grad Vršac", 
        "Bela Crkva", 
        "Kovin",
        "Grad Sombor",
        "Apatin",
        "Odžaci",
        "Kula",
        "Bač",
		"Bačka Palanka",
		"Bački Petrovac",
		"Beočin",
		"Bečej",
		"Vrbas",
		"Žabalj",
		"Grad Novi Sad",
		"Srbobran",
		"Temerin",
		"Titel",
		"Sremski Karlovci",
		"Grad Sremska Mitrovica",
		"Sremska Mitrovica",
		"Šid",
		"Inđija",
		"Irig",
		"Ruma",
		"Stara Pazova",
		"Pećinci",
		"Grad Šabac",
		"Grad Loznica",
		"Bogatić",
		"Vladimirci",
		"Koceljeva",
		"Mali Zvornik",
		"Krupanj",
		"Ljubovija",
		"Grad Valjevo",
		"Osečina",
		"Ub",
		"Lajkovac",
		"Mionica",
		"Ljig",
		"Grad Smederevo",
		"Smederevska Palanka",
		"Velika Plana",
		"Grad Požarevac",
		"Veliko Gradište",
		"Golubac",
		"Malo Crniće",
		"Žabari",
		"Petrovac na Mlavi",
		"Kučevo",
		"Žagubica",
		"Grad Kragujevac",
		"Aranđelovac",
		"Topola",
		"Rača",
		"Batočina",
		"Knić",
		"Lapovo",
		"Grad Jagodina",
		"Ćuprija",
		"Paraćin",
		"Svilajnac",
		"Despotovac",
		"Rekovac",
		"Grad Beograd"
		"Bor",
		"Kladovo",
		"Majdanpek",
		"Negotin",
		"Grad Zaječar",
		"Boljevac",
		"Knjaževac",
		"Sokobanja",
		"Grad Užice",
		"Bajina Bašta",
		"Kosjerić",
		"Požega",
		"Čajetina",
		"Arilje",
		"Priboj",
		"Nova Varoš",
		"Prijepolje",
		"Sjenica",
		"Grad Čačak",
		"Gornji Milanovac",
		"Lučani",
		"Ivanjica",
		"Grad Kraljevo",
		"Grad Novi Pazar",
		"Vrnjačka Banja",
		"Raška",
		"Tutin",
		"Grad Kruševac",
		"Varvarin",
		"Trstenik",
		"Ćićevac",
		"Aleksandrovac",
		"Brus",
		"Grad Niš",
		"Aleksinac",
		"Svrljig",
		"Merošina",
		"Ražanj",
		"Doljevac",
		"Gadžin Han",
		"Prokuplje",
		"Blace",
		"Kuršumlija",
		"Žitorađa",
		"Grad Pirot",
		"Bela Palanka",
		"Babušnica",
		"Dimitrovgrad",
		"Grad Leskovac",
		"Bojnik",
		"Lebane",
		"Medveđa",
		"Vlasotince",
		"Crna Trava",
		"Grad Vranje",
		"Vladičin Han",
		"Surdulica",
		"Bosilegrad",
		"Trgovište",
		"Bujanovac",
		"Preševo"]

minUserId = 308
maxUserId = 458

insertIntoProblem_Statement = "INSERT INTO `db_test`.`Problem` (`Adresa`, `Tip`, `Naslov`, `Opis`, `Datum`, `KorisnikId`, `Status`, `Opstina`) VALUES ("

with open('C:\\Users\\Toni\\si.19.18.probless\\Aplikacija\\Baza - skripte\\reci_100000.txt', 'r') as myfile:
    tekstFajla = myfile.read()
myfile.close()

for i in range(300):
    if (i%5==0):
        print("Doso sam do: " + str(i))
    if (i%2==0):
        r = requests.get(url = urlMale, verify = False)
    else:
        r = requests.get(url = urlFemale, verify = False)
    data = r.json()
    random_status = random.randint(0, 3)
    random_tip = random.randint(0, 23)
    random_opstina = random.randint(0, 117)
    random_user = random.randint(minUserId, maxUserId)
    random_pomeraj_naslova = random.randint(0, len(tekstFajla)-200)
    random_pomeraj_opisa = random.randint(0, len(tekstFajla) - 300)
    random_duzina_naslova = random.randint(30, 100)
    random_duzina_opisa = random.randint(60, 200)

	# adresa iz json se splituje, pa se uzima grad i adresa
    jsonAdresa=data["address"]
    listaAdresa = jsonAdresa.split()
    problemGrad = listaAdresa.pop()
    problemPostanskibroj = listaAdresa.pop()
    problemAdresa = ""
    for rec in listaAdresa:
        problemAdresa += rec
        problemAdresa += " "
    
	# postavljaju se random status, tip, opstina i korisnik
    problemStatus = status[random_status]
    problemTip = tip[random_tip]
    problemOpstina = opstine[random_opstina]
    problemUser = random_user

    # random datum
    problemDatum = str(radar.random_datetime(start='2019-01-01T00:00:00', stop='2019-06-07T00:00:00'))

	#generisanje naslova
    ind = 0
    problemNaslov = ""
    while (ind < random_duzina_naslova):
        problemNaslov += tekstFajla[random_pomeraj_naslova + ind]
        ind+=1

    problemNaslov = problemNaslov.replace("\"", " ")
    problemNaslov = problemNaslov.replace("\'", " ")
    problemNaslov = problemNaslov.replace("#", " ")
    problemNaslov = problemNaslov.replace("`", " ")

	# generisanje opisa
    ind = 0
    problemOpis = ""
    while (ind < random_duzina_opisa):
        problemOpis += tekstFajla[random_pomeraj_opisa + ind]
        ind+=1

    problemOpis = problemOpis.replace("\"", " ")
    problemOpis = problemOpis.replace("\'", " ")
    problemOpis = problemOpis.replace("#", " ")
    problemOpis = problemOpis.replace("`", " ")

    toWrite = insertIntoProblem_Statement
    toWrite += "\"" + problemAdresa + "\", \"" + problemTip + "\", \"" + problemNaslov + "\", \"" + problemOpis + "\", \"" + problemDatum + "\", " + str(problemUser) + ", \"" + problemStatus + "\", \"" + problemOpstina + "\");" 

    fw = codecs.open(filename, "a", "utf-8")
    fw.write(toWrite)
    fw.write("\n\n")
    fw.close()

    time.sleep(2)