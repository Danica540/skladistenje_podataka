import random
import requests
import json 
import urllib3
import codecs
import datetime
import radar
import time

filename = "C:\\Users\\Toni\\Desktop\\insertIntoOdgovor.txt"

insertIntoProblem_Statement = "INSERT INTO `db_test`.`Odgovori` (`Naslov`, `Sadrzaj`, `Datum`) VALUES ("

with open('C:\\Users\\Toni\\si.19.18.probless\\Aplikacija\\Baza - skripte\\reci_100000.txt', 'r') as myfile:
    tekstFajla = myfile.read()
myfile.close()

for i in range(80):
    if (i%5==0):
        print("Doso sam do: " + str(i))

    random_pomeraj_naslova = random.randint(0, len(tekstFajla)-200)
    random_pomeraj_sadrzaja = random.randint(0, len(tekstFajla) - 300)
    random_duzina_naslova = random.randint(30, 100)
    random_duzina_sadrzaja = random.randint(60, 200)

    # random datum
    odgovorDatum = str(radar.random_datetime(start='2019-01-01T00:00:00', stop='2019-06-07T00:00:00'))

	#generisanje naslova
    ind = 0
    odgovorNaslov = ""
    while (ind < random_duzina_naslova):
        odgovorNaslov += tekstFajla[random_pomeraj_naslova + ind]
        ind+=1

    odgovorNaslov = odgovorNaslov.replace("\"", " ")
    odgovorNaslov = odgovorNaslov.replace("\'", " ")
    odgovorNaslov = odgovorNaslov.replace("#", " ")
    odgovorNaslov = odgovorNaslov.replace("`", " ")

	# generisanje opisa
    ind = 0
    odgovorSadrzaj = ""
    while (ind < random_duzina_sadrzaja):
        odgovorSadrzaj += tekstFajla[random_pomeraj_sadrzaja + ind]
        ind+=1

    odgovorSadrzaj = odgovorSadrzaj.replace("\"", " ")
    odgovorSadrzaj = odgovorSadrzaj.replace("\'", " ")
    odgovorSadrzaj = odgovorSadrzaj.replace("#", " ")
    odgovorSadrzaj = odgovorSadrzaj.replace("`", " ")

    toWrite = insertIntoProblem_Statement
    toWrite += "\"" + odgovorNaslov + "\", \"" + odgovorSadrzaj + "\", \"" + odgovorDatum + "\");"

    fw = codecs.open(filename, "a", "utf-8")
    fw.write(toWrite)
    fw.write("\n\n")
    fw.close()

    time.sleep(2)