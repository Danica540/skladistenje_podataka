import random
import requests
import json 
import urllib3
import codecs
import datetime
import radar
import time

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

filename = "C:\\Users\\Toni\\Desktop\\insertIntoComments.txt"

minUserId = 308
maxUserId = 458

minProblemId = 235
maxProblemId = 805

insertIntoProblem_Statement = "INSERT INTO `db_test`.`Komentari` (`Sadrzaj`, `KorisnikId`, `ProblemId`, `Anoniman`, `Prioritet`, `Datum`) VALUES ("

with open('C:\\Users\\Toni\\si.19.18.probless\\Aplikacija\\Baza - skripte\\reci_100000.txt', 'r') as myfile:
    tekstFajla = myfile.read()
myfile.close()

for i in range(150):
    if (i%5==0):
        print("Doso sam do: " + str(i))
    
    random_user = random.randint(minUserId, maxUserId)
    random_problem = random.randint(minProblemId, maxProblemId)
    random_pomeraj_sadrzaja = random.randint(0, len(tekstFajla) - 300)
    random_duzina_sadrzaja = random.randint(60, 250)

    komentarUser = random_user
    komentarProblem = random_problem
    randomBool = random.randint(0, 2000) % 2
    komentarAnoniman = "false"
    if (randomBool == 1):
        komentarAnoniman = "true"

    # random datum
    komentarDatum = str(radar.random_datetime(start='2019-01-01T00:00:00', stop='2019-06-07T00:00:00'))

	# generisanje sadrzaja
    ind = 0
    komentarSadrzaj = ""
    while (ind < random_duzina_sadrzaja):
        komentarSadrzaj += tekstFajla[random_pomeraj_sadrzaja + ind]
        ind+=1

    komentarSadrzaj = komentarSadrzaj.replace("\"", " ")
    komentarSadrzaj = komentarSadrzaj.replace("\'", " ")
    komentarSadrzaj = komentarSadrzaj.replace("#", " ")
    komentarSadrzaj = komentarSadrzaj.replace("`", " ")

    toWrite = insertIntoProblem_Statement
    toWrite += "\"" + komentarSadrzaj + "\", " + str(komentarUser) + ", " + str(komentarProblem) + ", " + komentarAnoniman + ", " + str(0) + ", \"" + komentarDatum + "\");" 

    fw = codecs.open(filename, "a", "utf-8")
    fw.write(toWrite)
    fw.write("\n\n")
    fw.close()