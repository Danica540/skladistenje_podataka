import random
import codecs
import csv

filename = "insert_teams_sql.txt"

max_godina_osnivanja = 1999
min_godina_osnivanja = 1850

max_kapacitet_stadiona = 120000
min_kapacitet_stadiona = 40000

insertIntoTim_Statement = "INSERT INTO `mydb`.`tim` (`naziv`,`godina_osnivanja`,`kapacitet_stadiona`,`vlasnik`,`chairman`,`glavni_trener`,`fudbalski_stadion`,`spi`) VALUES ("
random_stadion = 0

with open('unique_teams.txt', 'r') as myfile:
    for line in myfile:
        naziv = line.replace('\n', "")
        kapacitet_stadiona = random.randint(
            min_kapacitet_stadiona, max_kapacitet_stadiona)
        godina_osnivanja = random.randint(
            min_godina_osnivanja, max_godina_osnivanja)
        vlasnik = ""
        chairman = ""
        glavni_trener = ""
        fudbalski_stadion = ""
        spi = random.uniform(0, 100)

        random_ime = random.randint(0, 1218)
        random_prezime = random.randint(0, 1218)

        a_file = open("imena.txt", encoding="utf8")

        for position, line_d in enumerate(a_file):
            if position == random_ime:
                line_d = line_d.lower()
                vlasnik += line_d + " "
        a_file.close()

        a_file = open("prezimena.txt", encoding="utf8")

        for position, line_d in enumerate(a_file):
            if position == random_prezime:
                line_d = line_d.lower()
                vlasnik += line_d
        a_file.close()

        vlasnik = vlasnik.title()

        random_ime = random.randint(0, 1218)
        random_prezime = random.randint(0, 1218)

        a_file = open("imena.txt", encoding="utf8")

        for position, line_d in enumerate(a_file):
            if position == random_ime:
                line_d = line_d.lower()
                chairman += line_d + " "
        a_file.close()

        a_file = open("prezimena.txt", encoding="utf8")

        for position, line_d in enumerate(a_file):
            if position == random_prezime:
                line_d = line_d.lower()
                chairman += line_d
        a_file.close()

        chairman = chairman.title()

        random_ime = random.randint(0, 1218)
        random_prezime = random.randint(0, 1218)

        a_file = open("imena.txt", encoding="utf8")

        for position, line_d in enumerate(a_file):
            if position == random_ime:
                line_d = line_d.lower()
                glavni_trener += line_d + " "
        a_file.close()

        a_file = open("prezimena.txt", encoding="utf8")

        for position, line_d in enumerate(a_file):
            if position == random_prezime:
                line_d = line_d.lower()
                glavni_trener += line_d
        a_file.close()

        glavni_trener = glavni_trener.title()

        a_file = open("stadioni.txt", encoding="utf8")

        for position, line_d in enumerate(a_file):
            if position == random_stadion:
                fudbalski_stadion = line_d
        a_file.close()
        random_stadion = (random_stadion+1) % 363

        ime_lige = ""
        with open('transferi.csv', 'r', encoding="utf8") as csv_file:
            csv_reader = csv.reader(csv_file)
            for line in csv_reader:
                if line[3] == naziv:
                    ime_lige = line[4]
                elif line[5] == naziv:
                    ime_lige = line[6]
        csv_file.close()

        # `vlasnik`,`chairman`,`glavni_trener`,`fudbalski_stadion`,`spi`
        toWrite = insertIntoTim_Statement
        toWrite += "\""+naziv+"\"" + ", " + str(godina_osnivanja) + ", " + "\""+str(kapacitet_stadiona) + "\"" + ", " + "\""+vlasnik+"\""+", " + \
            "\""+chairman+"\"" + ", " + "\""+glavni_trener + "\""+", " + "\"" + \
            fudbalski_stadion+"\"" + ", " + str(spi) + ");"
        fw = codecs.open(filename, "a", "utf-8")
        fw.write(toWrite)
        fw.write("\n\n")

myfile.close()
