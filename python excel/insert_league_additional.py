import random
import codecs
import mysql.connector
from mysql.connector import Error

filename = "insert_leagues_additional_sql.txt"

# UPDATE table_name
# SET column1 = value1, column2 = value2, ...
# WHERE condition;
updateLiga_Statement = "UPDATE `mydb`.`liga` SET "

with open('unique_leagues.txt', 'r', encoding="utf8") as myfile:
    for position, line in enumerate(myfile):
        naziv = line.replace('\n', "")
        idLige=position+1
        try:
            db = mysql.connector.connect(host='localhost',
                                         database='mydb',
                                         user='root',
                                         password='root')

            query = "SELECT COUNT(*) FROM mydb.fudbaler_has_tim WHERE fudbaler_has_tim.Liga_idLiga = " + str(
                idLige) + ";"

            cursor = db.cursor()
            cursor.execute(query)
            result = 0
            for obj in cursor:
                x = str(obj)
                x = x.replace("(", "")
                x = x.replace(")", "")
                x = x.replace(",", "")
                result = x
                print("position: " + str(idLige) + '  count: '+str(x))

            query = "SELECT SUM(spi) FROM (Select *FROM fudbaler_has_tim, tim  WHERE fudbaler_has_tim.Tim_idTim=tim.idTim) as tmp WHERE tmp.Liga_idLiga=" + \
                str(idLige) + ";"
            cursor.execute(query)
            sum_spi = 0
            for obj in cursor:
                x = str(obj)
                x = x.replace("(", "")
                x = x.replace(")", "")
                x = x.replace(",", "")
                sum_spi = x
                if(sum_spi == "None"):
                    sum_spi = 0.0
            avg = 0
            if float(result) != 0.0:
                avg = float(sum_spi) / float(result)
            toWrite = updateLiga_Statement
            toWrite += "broj_timova = "+str(result)+", srednja_vrednost_spi = "+str(
                avg) + " WHERE " + "liga.idLiga = "+str(position+1)+";"
            fw = codecs.open(filename, "a", "utf-8")
            fw.write(toWrite)
            fw.write("\n\n")

        except mysql.connector.Error as error:
            print("Failed MySQL: {}".format(error))
        finally:
            if (db.is_connected()):
                cursor.close()
                db.close()

            

myfile.close()
