import random
import codecs
import csv

filename = "insert_seasons_sql.txt"

insertIntoSezona_Statement = "INSERT INTO `mydb`.`sezona` (`datum_pocetka_sezone`,`datum_zavrsetka_sezone`,`prelazni_rok`,`ime_sezone`) VALUES ("

with open('unique_seasons.txt', 'r') as myfile:
    for line in myfile:
        line = line.replace("\n", "")
        ime_sezone = line
        years = line.split('-')
        random_month = 8
        random_date = random.randint(1, 10)
        star_date = str(years[0])+"-"+str(random_month)+"-"+str(random_date)

        random_month = 5
        random_date = random.randint(1, 20)
        end_date = str(years[1])+"-"+str(random_month)+"-"+str(random_date)

        prelazni_rok = "zimski"

        # `datum_pocetka_sezone`,`datum_zavrsetka_sezone`,`prelazni_rok`,`ime_sezone`
        toWrite = insertIntoSezona_Statement
        toWrite += "\""+star_date+"\"" + ", " + "\""+end_date+"\"" + ", " + \
            "\""+prelazni_rok + "\"" + ", " + "\""+ime_sezone+"\"" + ");"
        fw = codecs.open(filename, "a", "utf-8")
        fw.write(toWrite)
        fw.write("\n\n")

        prelazni_rok = "letnji"
        toWrite = insertIntoSezona_Statement
        toWrite += "\""+star_date+"\"" + ", " + "\""+end_date+"\"" + ", " + \
            "\""+prelazni_rok + "\"" + ", " + "\""+ime_sezone+"\"" + ");"
        fw = codecs.open(filename, "a", "utf-8")
        fw.write(toWrite)
        fw.write("\n\n")

myfile.close()
